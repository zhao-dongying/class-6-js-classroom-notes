### 正则表达式
用\d 匹配一个数字
```
'00\d'可以匹配'007',但无法匹配'00A'
'\d\d\d' 可以匹配'010'
```

用\w 可以匹配一个字母或数字
```
'\w\w' 可以匹配'js'
```

.可以匹配任意字符
```
'js.'可以匹配'jsp'、'jss'、'js!'
```

用*表示任意个字符(包括0个)
用+表示至少一个字符
用?表示0个或1个字符
用{n}表示n个字符
用{n，m}表示n-m个字符

例如: \d{3}\s+\d{3,8}
```
\d{3}表示匹配三个数字 比如'213'
\s+表示至少一个空格
\d{3,8}表示匹配3-8个数字
```


例：匹配010-12345
```
let regString=/^\d{3}\-\d{5}$/
let res=regString.test('010-12345')
console.log(res) 效果为true
```


```
要做更精确地匹配，可以用[]表示范围
[0-9a-zA-Z\_]可以匹配一个数字，字母或者下划线
[0-9a-zA-Z\_]+可以匹配至少由一个数字，字母或者下划线组成的字符串，比如'a100','0_Z','js2015'等等
[a-zA-Z\_\$][0-9a-zA-Z\_\$]*可以匹配由字母或下划线、$开头，后接任意个由一个数字，字母或者下划线、$组成的字符串，也就是JavaScript允许的变量名
[a-zA-Z\_\$][0-9a-zA-Z\_\$]{0,19}更精确地限制了变量的长度是1-20个字符(前面一个字符+后面最多19个字符)
```


```
A|B可以匹配A或B，所以(J|j)ava(S|s)cript 可以匹配'JavaScript','javascript','Javascript','javaScript'
^表示行的开头，^\d表示必须以数字开头
$表示行的结束，$\d表示必须以数字结束

```
