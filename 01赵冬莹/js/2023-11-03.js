//数组
// push()向Array的末尾添加若干元素
var arr=[1,9,18];
arr.push('你好',100,200)
console.log(arr);
console.log(arr.length);

// pop()则把Array的最后一个元素删除掉：
var num=arr.pop();
console.log(num); 
console.log(arr);

// sort()可以对当前Array进行排序，它会直接修改当前Array的元素位置，直接调用时，按照默认顺序排序：
var arr=['n','d','h','a','b']
arr.sort()
console.log(arr);

// splice()方法是修改Array的“万能方法”，它可以从指定的索引开始删除若干元素，然后再从该位置添加若干元素：
var arr=[1,9,18,5,21,52]
console.log(arr);
let res=arr.splice(0,5)
console.log(res)
console.log(arr)


var arr=[1,9,18,5,21,52]
let result1=arr.splice(1,0,100,200,300)
console.log(arr)


var arr=['赵','冬','莹']
let result=arr.splice(2,0,'今')
console.log(arr)

var arr=['可','以','吗']
let result2=arr.concat(['不','可','以'])
console.log(result2);

var result3=arr.join('-')
console.log(result3); 
var other=result3.split('-')
console.log(other);



//对象
//JavaScript用一个{...}表示一个对象，键值对以xxx: xxx形式申明，用,隔开。
//注意，最后一个键值对不需要在末尾加,，如果加了，有的浏览器（如低版本的IE）将报错。
var obj={
    name:'李白',
    age:23,
    height:178
}
console.log(obj);
console.log(obj.name);
console.log(obj['name']);
obj.name='小白'
obj.width=128
console.log(obj);
var res1='name' in obj;
console.log(res1);
var res2='grade' in obj;
console.log(res2);


//条件判断
//JavaScript使用if () { ... } else { ... }来进行条件判断。
var age=18;
var obj1={
    name:'赵冬莹',
    age:15
}
if(obj1.age>=age){
    console.log('这个同学长大了');
}else{
    console.log('小屁孩');
}


//for循环
var x=0;
var i;
for(i=1;i<5;i++){
    x=x+i;
}
console.log(x);



//作业
// 练习：如何通过索引取到500这个值：
var arr = [[1, 2, 3], [400, 500, 600], '-'];
var x = (arr[1][1]);
console.log(x);


// 练习：在新生欢迎会上，你已经拿到了新同学的名单，请排序后显示：欢迎XXX，XXX，XXX和XXX同学！：
var arr = ['小明', '小红', '大军', '阿黄'];
console.log('欢迎'+arr.slice(0,3)+'和'+arr[3]+'同学');





// 小明身高1.75，体重80.5kg。请根据BMI公式（体重除以身高的平方）帮小明计算他的BMI指数，并根据BMI指数：
// 低于18.5：过轻
// 18.5-25：正常
// 25-28：过重
// 28-32：肥胖
// 高于32：严重肥胖
// 用if...else...判断并显示结果：
var weight = parseFloat(prompt('请输入体重(kg):'));
var height = parseFloat(prompt('请输入身高(m):'));
var bmi=(weight/(height*height))
console.log(bmi);
if(bmi<18.5){
    console.log('过轻');
}else if(18.5<=bmi && bmi<25){
    console.log('正常');
}else if(25<=bmi && bmi<28){
    console.log('过重');
}else if(28<=bmi && bmi<32){
    console.log('肥胖');
}else {
    console.log('严重肥胖');
}



// 请利用循环遍历数组中的每个名字，并显示Hello, xxx!：
var arr = ['Bart', 'Lisa', 'Adam'];
var i, x;
for (i=0; i<arr.length; i++) {
    x = arr[i];
    console.log('Hello,'+x+'!');
}


// 1.数组求和
// 计算并返回给定数组 arr 中所有元素的总和
// 输入描述：
// [ 1, 2, 3, 4 ]
// 输出描述：
// 10
var arr=[1,2,3,4]
var total=0;
for(var i=0;i<=arr.length-1;i++){
    total=total+arr[i]
}console.log(total);



// 2.移除数组中的元素
// 描述
// 移除数组 arr 中的所有值与 item 相等的元素。不要直接修改数组 arr，结果返回新的数组
// 输入描述：
// [1, 2, 3, 4, 2], 2
// 输出描述：
// [1, 3, 4]
var arr2 = [1, 2, 3, 4, 2];
var arr2New = arr2.slice();
while (arr2New.length > 0) {
    if (arr2New.indexOf(2) !== -1) {
        arr2New.splice(arr2New.indexOf(2), 1)
    } else {
        break;
    }
}
console.log(arr2New);
 





// 3.移除数组中的元素 移除数组 arr 中的所有值与 item 相等的元素，直接在给定的 arr 数组上进行操作，并将结果数组返回
// 输入：
// [1, 2, 2, 3, 4, 2, 2], 2
// 输出：
// [1, 3, 4]
var arr3 = [1, 2, 2, 3, 4, 2, 2]
while (arr3.length > 0) {
    if (arr3.indexOf(2) !== -1) {
        arr3.splice(arr3.indexOf(2), 1)
    } else {
        break;
    }
}
console.log(arr3);



// 4.数组添加元素 描述
// 在数组 arr 末尾添加元素 item。结果返回新的数组。
// 注意：不要直接修改数组 arr!!!
// 输入描述：
// [1, 2, 3, 4],  10
// 输出描述：
// [1, 2, 3, 4, 10]
var arr3 = [1, 2, 3, 4]
var arr3New = arr3.slice();
arr3New.push(10);
console.log(arr3New);



// 5.删除数组 arr 最后一个元素。不要直接修改数组 arr，结果返回新的数组
var arr4 = [1, 2, 3, 4, 5]
var arr4New = arr4.slice()
arr4New.pop();
console.log(arr4New);

// 6. 添加元素描述
// 在数组 arr 开头添加元素 item。不要直接修改数组 arr，结果返回新的数组
// 输入：
// [1, 2, 3, 4], 10
// 输出：
// [10, 1, 2, 3, 4]
var arr6 = [1, 2, 3, 4]
        var arr6New = arr6.slice();
        arr6New.unshift(10);
        console.log(arr6New);



// 7. 删除元素 描述
// 删除数组 arr 第一个元素。不要直接修改数组 arr，结果返回新的数组
// 输入描述：
// [1, 2, 3, 4]
// 输出描述：
// [2, 3, 4]
var arr7 = [1, 2, 3, 4];
var arr7New = arr7.slice();
arr7New.shift();
console.log(arr7New);

// 8.数组合并 描述
// 合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组
// 输入描述：
// [1, 2, 3, 4], ['a', 'b', 'c', 1]
// 输出描述：
// [1, 2, 3, 4, 'a', 'b', 'c', 1]
var arr8_1 = [1, 2, 3, 4];
var arr8_2 = ['a', 'b', 'c', 1];
// var arr8New = arr8_1.slice();
// for (let i = 0; i < arr8_2.length; i++) {
//     arr8New.push(arr8_2[i]);
// }
// console.log(arr8New);
var arr8New = arr8_1.concat(arr8_2);
console.log(arr8New);




// 9.在指定位置添加元素
// 描述
// 在数组 arr 的 index 处添加元素 item。不要直接修改数组 arr，结果返回新的数组
// 示例1
// 输入：
// [1, 2, 3, 4], 'z', 2
// 输出：
// [1, 2, 'z', 3, 4]
var arr9 = [1, 2, 3, 4];
var arr9New = arr9.slice();
for (let i = 3; i < 5; i++) {
    arr9New[i] = arr9[i - 1];
}
arr9New[2] = 'z';
console.log(arr9New);


// 10.统计次数
// 描述
// 统计数组 arr 中值等于 item 的元素出现的次数
// 示例1
// 输入：
// [1, 2, 4, 4, 3, 4, 3], 4
// 输出：
// 3
var arr10 = [1, 2, 4, 4, 3, 4, 3];
var arr10New = arr10.slice();
var cont = 0;
for (let i = 0; i < arr10New.length; i++) {
    if (arr10New.indexOf(4) !== -1) {
        arr10New.splice(arr10New.indexOf(4), 1);
        cont += 1;
    } else {
        break;
    }
}
console.log(cont);



// 11.查找重复元素
// 描述
// 找出数组 arr 中重复出现过的元素（不用考虑返回顺序）
// 示例1
// 输入：
// [1, 2, 4, 4, 3, 3, 1, 5, 3]
// 输出：
// [1, 3, 4]
var arr11 = [1, 2, 4, 4, 3, 3, 1, 5, 3];
var arr11New = new Array;
arr11.sort()
for (let i = 0; i < arr11.length; i++) {
    if (arr11[i] === arr11[i + 1]) {
        arr11New.push(arr11[i]);
    }
    for (let i = 0; i < arr11New.length; i++) {
        if (arr11New[i] === arr11New[i + 1]) {
            arr11New.splice(arr11New.indexOf(arr11New[i]), 1)
        }
        arr11New[i]
    }
}
console.log(arr11New);


// 12.流程控制 描述
// 实现 fizzBuzz 函数，参数 num 与返回值的关系如下：
// 1、如果 num 能同时被 3 和 5 整除，返回字符串 fizzbuzz
// 2、如果 num 能被 3 整除，返回字符串 fizz
// 3、如果 num 能被 5 整除，返回字符串 buzz
// 4、如果参数为空或者不是 Number 类型，返回 false
// 5、其余情况，返回参数 num
function fizzBuzz(num) {
    if (num % 3 == 0 && num % 5 == 0) {
        return 'fizzbuzz';
    } else if (num % 3 === 0) {
        return 'fizz';
    } else if (num % 5 === 0) {
        return 'buzz';
    } else if (num === NaN || num != Number) {
        return false;
    } else {
        return num;
    }
}
let a = prompt('请输入num值')
console.log(fizzBuzz(a));


// 13.完全等同 描述
// 判断 val1 和 val2 是否完全等同
function name() {
    var judgment_a;
    var judgment_b;
    if (judgment_a === judgment_b) {
        return '他们完全相等';
    } else {
        return '他们不相等';
    }
}
console.log(name())