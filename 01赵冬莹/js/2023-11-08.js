function fun() {
    console.log('天才');
  }
  var fn=function(){
    console.log('一天');}
fn();


function fn(x){
    console.log(arguments);
    for (var item of arguments){
        console.log(item);
    }
    console.log('天才');
}
fn(18,21)

function lao(){
    console.log(arguments);
    console.log(`我是第一个参数${arguments[0]}`);
    console.log(`我是第二个参数${arguments[1]}`);
}
lao(10,20)


function lao(r){
    return 3.14*r*r;
}
let re=lao(3)
console.log(re);

//调用函数
function abs(x) {
    if (typeof x !== 'number') {
        throw 'Not a number';
    }
    if (x >= 0) {
        return x;
    } else {
        return -x;
    }
}
let res=abs(-5)
console.log(res);


function show(bookname,author){
    alert("书名："+bookname+"\n 作者："+author);
}
show("JavaScript从入门到精通","明日科技")



function student(name,age,sex){
    alert("姓名:"+name+"\n年龄:"+age+"\n性别:"+sex);
}
student("赵冬莹","21","女")



function max(a,b) {
    if (a > b) {
        return a;
    } else {
        return b;
    }

}
console.log(max(15, 20));