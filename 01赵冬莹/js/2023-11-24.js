let arr = [1, 2, 3, 4, 5, 7, 8, 9, 10, 15]
let r = arr.filter(function (x) {
    return x % 2 !== 0;
})
console.log(r);

let arr1 = [1, 2, 3, 4, 5, 7, 8, 9, 10, 15]
let r1 = arr1.filter(function (x) {
    return x % 2 === 0;
})
console.log(r1);


let arr2=[
    {
    name:'小黑',
    age:18
    },
    {
    name:'小粉',
    age:21
    },
    {
    name:'小红',
    age:32
    },
]

let r2=arr2.filter(function(x){
    return x.age>18;
})
console.log(r2);



let arr3=[1,2,3,4,7,9,2,3,8,4]
let r3=arr3.filter(function(val,idx,self){
    return self.indexOf(val)!==idx;
})
console.log(r3);

let arr4=[10,1,2,20]
let res=arr4.sort(function(x,y){
    if(x<y){
        return -1;
    }if(x===y){
        return 0;
    }return 1;
})
console.log(res);


let arr5=[1,2,10,30,40]
let res1=arr5.every(function(val){
    return val>0;
})
console.log(res1);

let arr6=[1,2,10,30,40]
let res2=arr6.every(function(val){
    return val>4;
})
console.log(res2);


let arr7=[11,12,13,14,15,16]
let res3=arr7.find(function(val){
    return val>13;
})
let res4=arr7.find(function(val){
    return val>16;
})
console.log(res3);
console.log(res4);

let arr8=[1,2,3,4,5,6]
let res5=arr8.findIndex(function(val){
    return val>2;
})
let res6=arr8.findIndex(function(val){
    return val>6;
})
console.log(res5);
console.log(res6);

'use strict';
var arr9 = ['Apple', 'pear', 'orange'];
arr9.forEach(console.log); // 依次打印每个元素