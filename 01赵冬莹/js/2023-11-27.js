function count() {
    var arr = [];
    for (let i=1; i<=3; i++) {
        arr.push(function () {
            return i * i;
        });
    }
    return arr;
}

var results = count();
var f1 = results[0];
var f2 = results[1];
var f3 = results[2];
console.log(f1());
console.log(f2());
console.log(f3());


function createCounter(init){
    var init = init || 0;//默认值，当init没有传入的时候，init的值为0
    return{
        increment:function(){
            return init+1;
        }
    }
}
let c1=createCounter();
let c2=createCounter(10);
console.log(c1.increment());
console.log(c2.increment());


let x=Math.pow(2,8);
console.log(x);



function createPow(y){
    function tmp(x){
        return Math.pow(x,y);
    }
    return tmp;
}
let pow2=createPow(2);
console.log(pow2(2));
console.log(pow2(3));
console.log(pow2(4));

let pow3=createPow(3);
console.log(pow3(1));
console.log(pow3(2));
console.log(pow3(3));


//for循环中的定时器 观察以下代码，提出修改方案，要求按预期输出1、2、3：
for(let i = 1; i < 4; i++) {
    setTimeout(function(){
      console.log(i);
    });
}

//简单乘法，实现方法mult，可接受1~2个参数

// 若参数中不包含回调函数，则返回一个包含mult方法的对象
// 若参数中包含回调函数，则执行回调函数
// 不需要考虑传入参数类型异常的情况
// 实现效果如下：
function mult(a,b){
    if(typeof(a)!=='function' && typeof(b)!=='function'){
        let obj ={
            mult : function(){
                
            } 
        } 
        return obj;
    }else {
        if(typeof(b)==='function'){
            b();
        }else if(typeof(a)==='function') {
            a();

        }
    }
}


/**
 * 实现栈结构
 */
function Stack() {
    let items = [];
    this.push = item => {
        items.push(item);
    }
    this.pop = () => items.pop();
}
let stack = new Stack();
console.log(stack.items);
stack.push(11);
let bbc=stack.pop();
console.log(bbc);
//经过测试发现问题。可以通过stack.items 直接访问和修改栈内的数据，请提出修改方案，要求不能直接访问items


//我们需要使用箭头符号（=>）来定义函数。箭头符号的左边是函数的参数，右边是函数体。
const myFunction = () => {
    // 函数体
  };

//单个参数的箭头函数：
const qqq=name=>{
    console.log(`hello,${name}`);
};
qqq("Alice");
//当函数只有一个参数时，可以省略参数周围的括号。



//隐式返回值的箭头函数：
const aa=(a,b)=>a*b
console.log(aa(2,5));
//当函数体只有一行代码时，你可以省略花括号，并且该行代码的结果将自动作为返回值。



//箭头函数作为回调函数：
const num=[1,2,3,4,5,6]
const twonum=num.map(num=>num*2)
console.log(twonum);
//在高阶函数中，如数组的 map() 方法，箭头函数常用作回调函数，用于对数组中的每个元素进行转换或处理。