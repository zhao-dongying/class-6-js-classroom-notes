var
    form = $('#test-form'),
    langs = form.find('[name=lang]'),
    selectAll = form.find('label.selectAll :checkbox'),
    selectAllLabel = form.find('label.selectAll span.selectAll'),
    deselectAllLabel = form.find('label.selectAll span.deselectAll'),
    invertSelect = form.find('a.invertSelect');

    //重置初始化状态
    form.find('*').show().off();
    form.find(':checkbox').prop('checked',false).off();
    deselectAllLabel.hide();

    //拦截form提交事件
    form.off().submit(function(e){
        e.preventDefault();
        alert(form.serialize());
    })

    //TODO：绑定事件
    selectAll.click(
        function(){
            if(this.checked){
                form.find(':checkbox').prop('checked',true);
                selectAllLabel.hide();
                deselectAllLabel.show();
            }else{
                form.find(':checkbox').prop('checked',false);
                selectAllLabel.show();
                deselectAllLabel.hide();

            }
        }
    );

    invertSelect.click(
        function(){
            form.find('[name=lang]').each(
                function(){
                    $(this).prop('checked',!$(this).prop('checked'));
                }
            );
            isAll();
        }
    );